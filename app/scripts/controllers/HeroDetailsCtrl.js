'use strict';

/**
 * @ngdoc function
 * @name klmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the klmApp
 */
angular.module('klmApp')
    .controller('HeroDetailsCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
        $scope.getCurrentHero = function(hero) {
            return hero.heroId == $stateParams.heroId;
        }
        $scope.currentHeroDetails = Object.assign({}, $scope.herosDetails.find($scope.getCurrentHero));
        $scope.saveChanges = function() {
            Object.assign($scope.herosDetails.find($scope.getCurrentHero), $scope.currentHeroDetails);
        }
    }]);
