'use strict';

/**
 * @ngdoc function
 * @name klmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the klmApp
 */
angular.module('klmApp')
    .controller('HeroesDashboardCtrl', ['$scope', '$state', function($scope, $state) {
        $scope.searchedHero = "";
        $scope.searchedHeroList = [];
        $scope.onHeroSearch = function() {
            if ($scope.searchedHero != "") {
                $scope.searchedHeroList = $scope.herosDetails.filter(function(hero) {
                    return hero.heroName.toLowerCase().indexOf($scope.searchedHero.toLowerCase()) != -1;
                });
            } else {
                $scope.searchedHeroList = [];
            }

        }

    }]);
