'use strict';

/**
 * @ngdoc function
 * @name klmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the klmApp
 */
angular.module('klmApp')
    .controller('MainCtrl', ['$scope', '$state', 'resolvedData', function($scope, $state, resolvedData) {
        $scope.herosDetails = resolvedData;
        $scope.moveto = function(stateToMove, heroId) {
            $state.go(stateToMove, { heroId: heroId });
        }
    }])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);
