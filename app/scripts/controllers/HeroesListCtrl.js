'use strict';

/**
 * @ngdoc function
 * @name klmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the klmApp
 */
angular.module('klmApp')
    .controller('HeroesListCtrl', ['$scope', function($scope) {
        $scope.heroToAdd = "";
        $scope.addHero = function() {
            if ($scope.herosDetails.length != 0) {
                var currentMaxID = Math.max.apply(Math, $scope.herosDetails.map(function(hero) {
                    return hero.heroId;
                }));
            } else {
                var currentMaxID = 0;
            }

            var newHeroToAdd = {
                heroId: currentMaxID + 1,
                heroName: $scope.heroToAdd
            }
            $scope.herosDetails.push(newHeroToAdd);
        }
        $scope.removeHero = function(heroToRemove) {
            $scope.heroToRemove = heroToRemove;
            var currentHeroToRemove = $scope.herosDetails.find($scope.getCurrentHeroToRemove);
            var indexToRemove = $scope.herosDetails.map(function(hero) {
                return hero.heroId;
            }).indexOf(currentHeroToRemove.heroId);
            $scope.herosDetails.splice(indexToRemove, 1);
            $scope.selectedHero = null;
        }
        $scope.getCurrentHeroToRemove = function(hero) {
            return $scope.heroToRemove.heroId == hero.heroId;
        }
        $scope.showHeroDetails = function(hero) {
            $scope.selectedHero = hero;
        }

    }]);
