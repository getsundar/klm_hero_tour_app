'use strict';

/**
 * @ngdoc overview
 * @name klmApp
 * @description
 * # klmApp
 *
 * Main module of the application.
 */
angular
    .module('klmApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ui.router'
    ])
    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('heroes', {
                name: 'heroes',
                url: '/heroes',
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main',
                resolve: {
                    resolvedData: function(heroServices) {
                        var data = heroServices.getHerosDetails();
                        return data;
                    }
                }
            })
            .state('heroesDashboard', {
                parent: 'heroes',
                name: 'heroesDashboard',
                url: '/heroesDashboard',
                templateUrl: 'views/templates/heroesDashboard.html',
                controller: 'HeroesDashboardCtrl',
                controllerAs: 'HeroesDashboard'
            })
            .state('heroesList', {
                parent: 'heroes',
                name: 'heroesList',
                url: '/heroesList',
                templateUrl: 'views/templates/heroesList.html',
                controller: 'HeroesListCtrl',
                controllerAs: 'HeroesList'
            })
            .state('heroesDetails', {
                parent: 'heroes',
                name: 'heroesDetails',
                url: '/heroesDetails/:heroId',
                templateUrl: 'views/templates/heroDetails.html',
                controller: 'HeroDetailsCtrl',
                controllerAs: 'HeroDetails'
            });
        $urlRouterProvider.otherwise('/heroes/heroesDashboard');
    });
